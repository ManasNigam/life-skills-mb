# Listening and Active Communication

###### What are the steps/strategies to do Active Listening? 

These are steps to do active listening-
* Face the speaker with eye contact.
* Pay attention. Give the speaker your undivided attention, and acknowledge the message.
* Show that you are listening.
* Always provide feedback to the speaker.
* Defer judgment.
* Respond Appropriately.


###### According to Fisher's model, what are the key points of Reflective Listening? 


Listeners encourage others to speak freely by taking a nonjudgmental and empathic approach. Copying the speaker's mood, reflecting the emotional state through words and nonverbal communication. This requires the listener to quiet his mind and concentrate completely on the speaker's mood.

###### What are the obstacles in your listening process?

There are some obstacles in listening process like -
* Lack of listening preparation
* Boredom
* Poorly structured and/or poorly delivered messages
* Sense of superiority
* Cognitive dissonance
* Impatience

###### What can you do to improve your listening?


* Consider making eye contact.
* Be alert.
* Take care of nonverbal things like body language and tone.
* Create a mental picture of what the speaker is saying.
* Feel sympathy for the speaker.
* Please provide feedback.
* Maintain an open mind.



###### When do you switch to Passive communication style in your day to day life?
In these conditions i switch to passive communication -
* If i have different opinion or feelings.
* If i am protecting my rights, and 
* Identifying and meeting my needs

###### When do you switch into Aggressive communication styles in your day to day life?

In these condtions i switch to agressive communication -
* If my rights are violated.
* If someone blame me for that thing i haven't done.
* If someone is not behaving well with other.


###### How can you make your communication assertive? 

* Assess you style in which things you are comfortable .
* Don't hesitate to say no.
* First think about what you say if it not get hurt to the listner.
* Keep your emotion in check.





