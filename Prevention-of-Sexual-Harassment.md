# Prevention of Sexual Harassment

##### What kinds of behaviour cause sexual harassment?

Kind of behaviours cause sexual harassment are:
* unwanted kissing, touching, slapping
* making sexually explicit comments, uninvited messages
* requests for sexual favours
* sexually suggestive gestures
* ogling
* catcalls or cornering someone in a tight space, etc. 

These all are come under sexual harassment case.Any of the mentioned behaviours could be considered sexual harassment if they occur frequently, are serious enough to frighten or unnerve a worker, or are obvious enough to get in the way of their work.


##### What would you do in case you face or witness any incident or repeated incidents of such behaviour?

Federal law prohibits two specific types of sexual harassment.

These two types of harassment are hostile work environment harassment and quid pro quo harassment, which occurs when receiving something in exchange for your cooperation with a sexual request (example: "If you go on a date with me, I'll give you more hours") (unwanted touching, suggestive texts or emails, or sharing sexually explicit images or videos).

It's crucial to keep detailed records of all instances of harassment, regardless of the kind you're dealing with. Detail information like:

* What happened, what was said, and who saw the behaviour, as well as the date, time, and location of the harassment.
* Any pertinent emails, texts, pictures, or social media posts should be saved as copies or screenshots.
* Inform a dependable friend, relative, or coworker of what occurred, and record the specifics of your interactions. In addition to offering support, they might also be able to offer you confirming statements if you need them.

