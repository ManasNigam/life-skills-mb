# Grit and Growth Mindset

## 1. Grit
##### Paraphrase (summarize) the video in a few lines. Use your own words.

 

* Most intelligent person  is depends on whose iq is good, but in real life how you tackle problems shows that.

* Grit is passion and perseverence for long term goals, grit is sticking with you future for years and working very hard
,building grit in people is like changing mindset challenging them that you can do this things.

##### What are your key takeaways from the video to take action on?


Grit is passion and perseverence for long term goals, grit is sticking with you future for years and working very hard
,building grit in people is like changing mindset challenging them that you can do this things.



## 2. Introduction to Growth Mindset

##### Paraphrase (summarize) the video in a few lines in your own words.

* People mindset plays a cruicial role in being a successful.
* There are two types of mindsets are Fixed and Grwoth mindset.
* Fixed mindset people believe that you are not in control of your abilities.Skills are born.You can't learn and grow.
* Whereas growth mindset people believes that you are in control of your abilities.Skills can be built and you can grow and learn.
* Learn Foundation of anything if foundation is good then you can grow easily.
  


##### What are your key takeaways from the video to take action on?

* We are just one or the other in different time you be in growth or fixed we can identify our mindset fixed or growth.

* We have to create great culture of learning.

## 3. Understanding Internal Locus of Control

##### What is the Internal Locus of Control? What is the key point in the video?


* Locus of control refers to the extent to which people feel that they have control over the events that influence their lives.
* When we are dealing with challenges in our life then we see that how our locus of control is good.
  
## 4. How to build a Growth Mindset

##### Paraphrase (summarize) the video in a few lines in your own words.
* Belive in urself that you can figure out things and challenges.
* Do not quit. 
* Dont say everyone is god gifted who do anything.
* Say i belive i can figure out today take a break then bounce back. 
* Belive in yourself that you can do anything challenge yourself.




##### What are your key takeaways from the video to take action on?

* Always honour the struggle you have made in you process.
* Don't hate process and quit. 
* It is difficult right now but say i am on it,this is a part of process challenge yourself.



## 5.Mindset

##### What are one or more points that you want to take action on from the manual? 


I am very good friends with Confusion, Discomfort and Errors.
* Confusion tells me there is more understanding to be achieved.
* Discomfort tells me I have to make an effort to understand. I understand the learning process is uncomfortable.
* Errors tell me what is not working in my code and how to fix it.

I will follow the steps required to solve problems:
* Relax
* Focus - What is the problem? What do I need to know to solve it?
* Understand - Documentation, Google, Stack Overflow, Github Issues, Internet
* Code
* Repeat.
