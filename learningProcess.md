## Learning Process

#### How to Learn Faster with the Feynman Technique

###### What is the Feynman Technique?


Richard Feynman was a Nobel prize-winning physicist who founded the Feynman technique.He won the noble prize in 1965 for his work in quantum electrodynamics.
His superpowers are that he can explain complicated theory or subjects to others in simple terms.

Four key steps of Feynman Technique are -
* Take a piece of paper and write down the name of concept you want to learn.
* Explain it to a child in simple language.
* Identify the areas where you stuck and understand that.
* Challenge yourself to do the task in limited time.

Feyman Technique helps us to identify where you are strong and on which topic you are shaky.

###### What are the different ways to implement this technique in your learning process?

We can implement it to our daily learnings like if we are not confident in any subject note down all the topics related where you want to need practice,practice it and then explain it to some child in much easier way that anyone can understand.
Then again mark the topics in which you are confident and shaky.
Repeat these step untill you have full confidence in subject.


#### Learning How to Learn TED talk by Barbara Oakley

###### Paraphrase the video in detail in your own words

Barbara Okley is an American Professor of engineering at Okland University.
Oakley created and taught Learning How to learn.
Barbara Oakley said that everyone should have a curiosity to learn anything.  
She talks about two learning modes which are -
* Focused Mode 
* Diffused Mode

In Focused mode you are paying your full attention of brain.
When you are focusing on something,your some part of brain engaging with some work.

In Diffused mode your mind is relaxed and free.When you are in diffused mode you are using your mostly different part of brain you use in focued mode.

Barbara explained about Pinball technique is very useful in explaining diffused mode and focused mode of learning.
Salvor Dali is ver famous artist he used a technique when he get stuck in any problem he just put his mind in relax state with work aside and wake up in a spontaneous way such that his mind get charged and get creative ideas and solution for the problem.
Relaxation of mind is as important as to find solution for any problem.

Use Pomodoro Technique to learn anything. In this technique pick a task do it with focus for 25 minutes and then take a break of 5 minutes and then repeat, it is very useful it just helps us to learn with less load of presssure.

Do excercises and test yourself do not only understand practice it too, take short breaks as relaxation is very important.

###### What are some of the steps that you can take to improve your learning process?

* I will implement pomodoro technique in my daily learning basis.
* I will practice things not only understand it.
* I will use focuse mode of learning without engaging my brain into another work.
* I will try to note down the hinderences which comes in m learning process and try to overcome these.  

##### Learn Anything in 20 hours

###### Your key takeaways from the video? Paraphrase your understanding.


* We can learn any new skills in just complete 20 hrs whether it is for 20 hrs a day or 45 min for a month.
* So we can learn any new skills in these 20 hrs with good and concised resources.
* Always put a problem into pieces or anything you want to learn put into pieces.
* Learn only those things which are necessary.
* Practice it with removal of distractions or barriers.

###### What are some of the steps that you can while approaching a new topic?

* Firstly i will collect all the necessary resources for learning that skills.
* And then i will set a time limit for everyday fro every topic in this skill using pomodoro technique.
* Deconstruct the skills break down it in smaller pieces of skill and then practice them.
* Practice at least 20 hrs for any skill.

Major barrier to learn any skill is not intellectual but emotional.
But put 20 hrs in to it and we will sure get results.