# Energy Management

### 1. Manage Energy Not Time

##### Q1. What are the activities you do that make you relax - Calm quadrant?

* For relaxing first thing I do is to give my body full rest after work.
* For peace I usually sleep or just go for walk outside.
* For relaxation I play mobile games also.
* But most of the time I am very relaxed when I am walking in the park and observing nature.

##### Q2. When do you find getting into the Stress quadrant?

* When I am irritated very easily I find myself falling into the stress quadrant.
* When I get angry at even a small thing.
* In these situations I feel my body lazy.

##### Q3. How do you understand if you are in the Excitement quadrant?

* When I am in the excitement quadrant I feel very happy from inside,
* I find myself in the excitement quadrant when I am enthusiastic about something.


## Sleep is your superpower


##### Q4. Paraphrase the Sleep is your Superpower video in detail.

Sleep is very important for any person as we are giving full rest to our body after work.

It is discovered that you need sleep after learning to save memory, but it is also discovered that you also need sleep before learning because it will prepare your mind for new challenges.

When you get proper sleep your brain is 40% more efficient than in normal ways. Sleep is very important for good health. If you sleep only 4hr a day your immunity dropped by 70% which leads to many cardiovascular problems. As it will directly affect your immune system.


##### Q5. What are some ideas that you can implement to sleep better?


Tips for good sleep:
* Regularity - Keeping it regular like getting up and going to bed at the same time daily improves the quality and quantity of sleep.
* Keep it cool - The body needs to drop some temperature to go into a sleep state, and do sleep at optimal room temperature.

Sleep is a non-negotiable biological asset in our life system.

## Brain-Changing Benefits of Exercise



##### Q6. Paraphrase the video - Brain Changing Benefits of Exercise. Minimum 5 points.

* exercise is the most transformative thing for the brain. Exercise changes your brain this is proved by discoveries.
* Exercise is very important for the brain because of three reasons.
* First reason is a single workout increases the level of neurotransmitters like dopamine and serotonin in our body which directly increase focus and attention.
* Second reason is workout increases the reaction time of your body.
* And the third exercise changes brain anatomy analogy and function.
* if you get 3 to 4 times a week 30 mins an exercise it is good.
* Bringing exercise will not give you happier life today but it will protect your brain from an incurable disease and it will change the trajectory of your life.


##### Q7. What are some steps you can take to exercise more?


* You can set a time for exercise, you don't have to work out for 2-3 hrs a day, only do for 45 mins a day and 3-4 days a week thats enough for your brain to grow.
* See motivation videos to motivate yourself.
