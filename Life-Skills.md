# Elasticsearch, Solr and Lucene  

#### Abstract  

It takes lots of our time and energy to obtain data from several data sources because of the rising demand for data storage. As a result, we would like a system for querying data that is highly accessible, includes a high capacity, and might readily expand up without requiring the addition of additional hardware to one device. Within the paper, we discuss Elasticsearch, one such robust full-text search and analytics engine. Structured, unstructured, geographical, graphical, and numerical data are just some of the several varieties of data that Elasticsearch is created to handle. It absolutely was enhanced with better functionality and developed on top of Lucene. Several technologies that provide a visualization platform, processing pipeline, data science, and machine learning help to extend the facility of Elasticsearch.  

 
#### Introduction  


* Elasticsearch(ES) 

    Elasticsearch could be powerful analytics and full-text programme. It is a distributed, open-source program that will handle a spread of knowledge, including alphabetic, numeric, structured, and unstructured data. On top of Lucene, it is a distributed document store system. it is claimed to be a highly available program whose scaling is straightforward because of its distributed structure. It provides us with a REST API that is JSON-based, allowing us to reference Lucene capabilities. It enables us to hold out some complicated data aggregations, a few which Lucene is unable to implement. It is claimed to be a highly available programme whose scaling is straightforward because of its distributed structure. It provides us with a REST API that is JSON-based, allowing us to reference Lucene capabilities. It enables us to hold out some complicated data aggregations, a few which Lucene is unable to implement. While Lucene. It only needs a modest heap of 1 GB, Elasticsearch could be a resource-hungry programme that needs lots of heap space. Nevertheless, the difficulty is fixed by either freezing the shards or spreading the load across several nodes. Since Lucene and Elasticsearch have a relationship such as that of a car and its engine, we may argue that the earlier is propelled by the latter.  

<br> 

* Solr 

    The Java-based Solr platform is free and open-source for enterprise search. Some of its primary features include full-text search, hit highlighting, faceted search, dynamic clustering, real-time indexing, database integration, NoSQL features, and handling of rich documents (such as Word and PDF). Solr is built for fault tolerance and scalability, and it offers distributed search and index replication. With a vibrant development community and frequent releases, Solr is a popular choice for enterprise search and analytics use cases. 
    Running independently, Solr is a full-text search server. Its full-text indexing and search are powered by the Lucene Java search library, and most popular programming languages can use it thanks to its REST-like HTTP/XML and JSON APIs. Due to its external configuration, Solr can be adapted for a variety of non-Java applications. 

<br> 

##### Solr vs. Elasticsearch: Performance Differences & More. How to Decide Which One Is Best for You   

![img](https://dattell.com/wp-content/uploads/2020/10/Solr-vs-Elasticsearch-2048x1138.png)  

 
Although they are both based on the same fundamental search library, Lucene, they differ in terms of features like scalability, ease of deployment, community presence, and many other things. Due to its caches and the ability to facet and sort using an uninverted reader, Solr offers greater benefits when dealing with static data, such as in e-commerce. Elasticsearch, on the other hand, is significantly more widely used and ideally suited for time series data use cases like log analysis. There is no right or wrong decision when deciding between Solr and Elasticsearch because each has its own set of advantages and disadvantages.  
These are mentioned below -  

<br> 

##### 1. Search Feature  

We felt confident that either option would provide us with the fundamental search functionalities we required because ES and Solr are both built on Lucene. We must consider the merits and drawbacks of each system, though, because of their respective histories and design priorities. Solr has historically concentrated on finding solutions to challenging information retrieval (IR) issues. Its API, which exposed more potent features than ES, reflected this. In contrast, ES was a bit behind IR in terms of features since it had been developed with an emphasis on flexibility (hence the name). In our situation, we did not anticipate using many of the more sophisticated capabilities right away. Despite having the technological edge, Solr.  

 
##### 2. Search Scalability

One of the main issues we had with Solr was how immature many of the new capabilities were when they first appeared in SolrCloud, especially the Overseer/Queue management. When using the SolrCloud release "out of the box," we encountered some serious stability issues. During testing, the cluster experienced total lock-ups that could only be fixed by a complete reset. In contrast, ES breezed through the same tests without experiencing any irrecoverable failures. Yes, we could make ES lose data on purpose. We knew precisely how it happened, though, and we were sure we could engineer our way out of such predicaments.  


##### 3. Performance  

Although ES and Solr are both built on Lucene, our performance testing revealed variances in how each system uses Lucene. The following graph shows that Solr was the undisputed champion in terms of indexing:  

 
![Performance graph](https://www.loggly.com/wp-content/uploads/2014/07/Elasticsearch-vs-Solr.jpg)  


Each data point represents a single test in which we index batches of 8,000 events over a predetermined amount of time (2, 5, 10, 15 and 20 minutes, left to right). You can observe that the results clustered into comparatively distinct groupings. While ES began at around the same pace (albeit with more fluctuation) and progressively slowed down to approximately 12,000 eps as the test runs became longer, Solr consistently indexed at about 18,000 eps.  
Although it appears like Solr has won easily, a massive part of the difference can be attributed to the fact that Solr was running Lucene 4 while ES was using 3.6.  

 
##### 4. Community

Open-source projects ES and Solr both have vibrant communities. We spoke about the various philosophical frameworks that each employed and decided that we liked Solr's more open framework better, but we were also pleased by the ES team's leadership. Solr had the edge in terms of both size and activity of the community surrounding each project. However, ES was expanding, and rapidly.  


##### 5. Other Decision Factors  

* The ES API was beautiful, strong, and much closer to a REST service.  
* We found several applications for the ES scan and percolate characteristics, which were both fascinating.  
* Native hierarchical/pivot facets in Solr4 and automatic typing in ES are both excellent approaches to avoid managing a continually changing input stream. Nice!  
* The ES experience right out of the box is significantly easier than Solr's Plug-in support is good for both, although Solr offered "deeper" plugins.  


##### Our Decision  
 
I believe it would be tougher to choose the search engine now. Because of its caches and the ability to facet and sort data using an uninverted reader, Solr has more advantages when it comes to static data, such as e-commerce. Elasticsearch, on the other hand, is much more frequently used and better suited for timeseries data use cases like log analysis.


#### References  

- [Solr vs. Elasticsearch: Performance Differences & More. How to Decide Which One Is Best for You](https://sematext.com/blog/solr-vs-elasticsearch-differences/#:~:text=Solr%20has%20more%20advantages%20when,like%20log%20analysis%20use%20cases.)  

 
 

- [Elasticsearch Vs Solr](https://www.hcltech.com/blogs/elasticsearch-vs-solr)  

 
 

- [Why Loggly Chose Elasticsearch Over Solr](https://www.loggly.com/blog/loggly-chose-elasticsearch-reliable-scalable-log-management/)  

 
 

- [Compare Apache Solr and Elastic Observability](https://www.g2.com/compare/apache-solr-vs-elastic-observability)  

 
 

- [Markdown Syntax Guide](https://docs.github.com/en/get-started/writing-on-github/getting-started-with-writing-and-formatting-on-github/basic-writing-and-formatting-syntax) 

 